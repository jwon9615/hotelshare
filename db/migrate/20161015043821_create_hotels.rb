class CreateHotels < ActiveRecord::Migration
  def change
    create_table :hotels do |t|
      t.string :name
      t.string :address
      t.string :rating
      t.string :rating_count

      t.timestamps null: false
    end
  end
end
