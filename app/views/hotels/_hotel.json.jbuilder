json.extract! hotel, :id, :name, :address, :rating, :rating_count, :created_at, :updated_at
json.url hotel_url(hotel, format: :json)