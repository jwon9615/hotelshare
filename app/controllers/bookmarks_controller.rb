class BookmarksController < ApplicationController

	def new
		@bookmark = Bookmark.new
		
	end

	def create
		@bookmark = Bookmark.new(bookmark_params)
		if @bookmark.save 
			redirect_to root_path
		else
			redirect_to root_path
		end

		
	end

	private

		def bookmark_params
			params.require(:bookmark).permit(:user_id, :hotel_id)
			
		end
end
