class StaticPagesController < ApplicationController
  def home

	  @q = Hotel.ransack(params[:q])
	  @hotels = @q.result(distinct: true)

  	@all_hotels = Hotel.all
  	@all_events = Event.all

  	@bookmark = Bookmark.new
  	
  end
end
