class Hotel < ActiveRecord::Base

	def get_hotel
		Hotel.all.each do |h|
			@hotels["name"] = h.name
			@hotels["address"] = h.address
		end
	end

	def self.assign_headers  # API Call starts here

	query = {  #Copied this from the Request Body on the Sabre Dev Studio site

    "GetHotelAvailRQ": {

        "SearchCriteria": {

            "GeoRef": {

                "RefPoint": {

                    "CountryCode": "SG",

                    "RefPointType": "6",

                    "ValueContext": "CODE",

                    "Value": "SIN"

                },

                "Radius": 40.0,

                "UOM": "MI",

                "Category": "HOTEL"

            },

            "RateInfoRef": {

                "CurrencyCode": "USD",

                "StayDateRange": {

                    "StartDate": "2016-10-21",

                    "EndDate": "2016-10-22"

                },

                "GuestCount": {

                    "Count": 2

                }

            },

            "HotelPref": {



            },

            "ImageRef": {

                "Type": "SMALL",

                "LanguageCode": "EN"

            }

        }

    }

}


		headers = {  # Copied this from the Request Headers from the Sabre Dev Studio Site
  			"Authorization" => "Bearer T1RLAQJ1e+XtyGArgjGsOIRgVlzaw6vVvhDCAGHKpkgC+Tv2ZCSix5DWAACgP8W0s9lG9D1c/0Nd/fAbVQ3hGi19R34p/0C4Y/YwEotqH2YQ+pHaCyJ32jhIrZXyfDvw/f0NfHf5mv0no1PhWfQWxg27I8e7ln1G5K4JX3X+OhOgwujLDVHQ3+C6U4cHwf0cMifY1ENk9fOye4IA6jR+49/zzVNiMtHIE7M06Hl173KPZGqFCfDkWtcoiyphHpMEnoffHg34vvPt0mXSiA**",
  			"X-Originating-Ip" => "122.11.135.54",
  			"Content-Type" => "application/json" 
		}

		@response = HTTParty.post( # this is a rails specific gem that consolidates everything and makes the call to Sabre API
  			"https://api.test.sabre.com/v1.0.0/shop/hotels?mode=avail", 
  			:body => query.to_json,
  			:headers => headers
		)

		@response["GetHotelAvailRS"]["HotelAvailInfos"]["HotelAvailInfo"] #this returns a formatted response, getting the specific information on the hotel availability

	end

	def self.hotel_info(hotelcode)


		query = {

		    "GetHotelContentRQ": {

		        "SearchCriteria": {

		            "HotelRefs": {

		                "HotelRef": [{

		                    "HotelCode": hotelcode

		                }, {

		                    "HotelCode": hotelcode

		                }]

		            },

		            "DescriptiveInfoRef": {

		                "PropertyInfo": true,

		                "LocationInfo": true,

		                "Amenities": true,

		                "Descriptions": {

		                    "Description": [{

		                        "Type": "Dining"

		                    }, {

		                        "Type": "Alerts"

		                    }]

		                },

		                "Airports": true,

		                "AcceptedCreditCards": true

		            },

		            "ImageRef": {

		                "MaxImages": "10"

		            }

		        }

		    }

		}


		headers = { 
  			"Authorization" => "Bearer T1RLAQJ1e+XtyGArgjGsOIRgVlzaw6vVvhDCAGHKpkgC+Tv2ZCSix5DWAACgP8W0s9lG9D1c/0Nd/fAbVQ3hGi19R34p/0C4Y/YwEotqH2YQ+pHaCyJ32jhIrZXyfDvw/f0NfHf5mv0no1PhWfQWxg27I8e7ln1G5K4JX3X+OhOgwujLDVHQ3+C6U4cHwf0cMifY1ENk9fOye4IA6jR+49/zzVNiMtHIE7M06Hl173KPZGqFCfDkWtcoiyphHpMEnoffHg34vvPt0mXSiA**",
  			"X-Originating-Ip" => "122.11.135.54",
  			"Content-Type" => "application/json" 
		}

		@httpresponse = HTTParty.post(
  			"https://api.test.sabre.com/v1.0.0/shop/hotels/content?mode=content", 
  			:body => query.to_json,
  			:headers => headers
		)

		dictionaryhotel = Hash.new
		if !@httpresponse["GetHotelContentRS"]["HotelContentInfos"]["HotelContentInfo"].nil?
			@httpresponse["GetHotelContentRS"]["HotelContentInfos"]["HotelContentInfo"].each do |h|
	          dictionaryhotel["Address"] = h["HotelDescriptiveInfo"]["LocationInfo"]["Address"]["AddressLine1"]
	          dictionaryhotel["City"] = h["HotelDescriptiveInfo"]["LocationInfo"]["Address"]["CityName"]
	        end
	     end

        return dictionaryhotel

	end
		


end
